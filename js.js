//hack to prevent page reload--------
const aBtns = document.querySelectorAll('.drop-down>a')

aBtns.forEach(function (a) {
    a.addEventListener('click', function (e) {
        e.preventDefault();
    })
})
//------------------------------------


//drop down menu---------------------------------------------

const dropDown = document.querySelectorAll('.drop-down')

dropDown.forEach(function (drop) {
    drop.addEventListener('click', function () {
        if (!drop.lastElementChild.classList.contains('active')) {
            this.lastElementChild.classList.add('active')
        } else {
            this.lastElementChild.classList.remove('active')
        }
    })
})

//---------------------------------------------------------

//burger--------------------------------------------------
const burger = document.querySelector('.burger');
burger.addEventListener('click', function () {
    burger.nextElementSibling.classList.toggle('active');
    dropDown.forEach(drop => {
        if (drop.lastElementChild.classList.contains('active')) {
            drop.lastElementChild.classList.remove('active');
        }
    })
})

burger.addEventListener('click', function () {
    this.classList.toggle('open');
})
//--------------------------------------------------------------

//MAGIC HAPPENS - showing menu after scroll-------------------
const menu = document.querySelector('.menu');
const logo = document.querySelector('.company-logo');
const phone = document.querySelector('.phone-icon ');

function showMenuAfterScroll() {
    if (window.pageYOffset > window.innerHeight / 1.1) {
        menu.classList.add('show-after-scroll')
        logo.style.display = "block";
        phone.style.display = 'none';

    } else {
        menu.classList.remove('show-after-scroll')
        phone.style.display = 'block';

    }
}

function myFunction(x) {
    if (x.matches) {
        // If media query matches
        window.addEventListener('scroll', showMenuAfterScroll)
        if (menu.classList.contains('active')) {
            menu.classList.remove('active')
        }
    } else {
        window.removeEventListener('scroll', showMenuAfterScroll);
        menu.classList.remove('show-after-scroll')
        logo.style.display = "none";
    }
}
var x = window.matchMedia("(min-width: 1300px)");
myFunction(x); // Call listener function at run time
x.addListener(myFunction);



//light----------------------------------
// Constants

const LIGHT_COLOR = [220, 255, 255];
const LIGHT_INTENSITY = 3;
const LIGHT_SPREAD = 10;
const LIGHT_BLUR = 20;
const ANIMATION_DELAY = 10;

const CONTAINER_ELEMENT_SELECTOR = '.light-menu';
const MAIN_ELEMENT_SELECTOR = '.light-menu__main';
const ITEM_ELEMENT_SELECTOR = '.light-menu__item';

// Classes

class Point {

    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

}

class LightRay {

    constructor() {
        this.center = new Point();
        this.edgeA = new Point();
        this.edgeB = new Point();
    }

}

class Color {

    constructor(r = 0, g = 0, b = 0, a = 1) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    toString() {
        return 'rgba(' + this.r + ', ' + this.g + ', ' + this.b + ', ' + this.a + ')';
    }

}

class LightController {

    constructor() {
        this.containerElement = null;
        this.mainElement = null;
        this.itemElements = null;
        this.lightColor = new Color(255, 255, 255);
        this.lightIntensity = 1;
        this.lightSpread = 10;
        this.lightBlur = 10;
        this.animationDelay = 10;
    }

    run() {
        this.ensureElementsExist();
        this.setElementsPosition();
        this.bindEvents();
        this.createCanvasElements();
        this.createLight();
        this.runLoop();
    }

    runLoop() {
        this.updateLight();
        this.drawLight();
        window.requestAnimationFrame(() => {
            this.runLoop();
        })
    }

    ensureElementsExist() {
        if (!this.containerElement || !this.mainElement || !this.itemElements) {
            throw new Error('Elements are not defined');
        }
    }

    setElementsPosition() {
        let centerX = this.containerElement.offsetWidth / 2;
        let centerY = this.containerElement.offsetHeight / 2;
        let itemsLength = this.itemElements.length;
        this.mainElement.style.left = centerX + 'px';
        this.mainElement.style.top = centerY + 'px';
        this.itemElements.forEach((element, index) => {
            element.style.left = (Math.sin(index / itemsLength * 2 * Math.PI) * centerX + centerX) + 'px';
            element.style.top = (Math.cos(index / itemsLength * 2 * Math.PI) * centerY + centerY) + 'px';
        });
    }

    bindEvents() {
        this.itemElements.forEach(itemElement => {
            itemElement.addEventListener('mouseover', event => {
                this.selectElement(event.target);
            });
            itemElement.addEventListener('mouseout', () => {
                this.deselectElement();
            });
        });
    }

    createCanvasElements() {
        this.canvasElements = [];
        for (let i = 0; i < this.lightIntensity; i++) {
            let canvasElement = document.createElement('canvas');
            canvasElement.width = document.documentElement.clientWidth;
            canvasElement.height = document.documentElement.clientHeight;
            canvasElement.style.position = 'absolute';
            canvasElement.style.mixBlendMode = 'overlay';
            canvasElement.style.left = 0;
            canvasElement.style.top = 0;
            canvasElement.style.width = '100%';
            canvasElement.style.height = '100%';
            this.containerElement.parentNode.insertBefore(canvasElement, this.containerElement);
            this.canvasElements.push(canvasElement);
        }
    }

    createLight() {
        this.light = new LightRay();
        this.targetLight = new LightRay();
        this.resetLight(this.light);
        this.resetLight(this.targetLight);
    }

    selectElement(element) {
        let corners = this.getElementCorners(element);
        let center = this.getCenter();
        let maxAngle = 0;
        corners.forEach(pointA => {
            corners.forEach(pointB => {
                let angleAC = Math.atan2(pointA.y - center.y, pointA.x - center.x) / Math.PI * 180;
                let angleBC = Math.atan2(pointB.y - center.y, pointB.x - center.x) / Math.PI * 180;
                let angleABC = Math.abs(angleAC - angleBC);
                if (angleABC > maxAngle) {
                    maxAngle = angleABC;
                    this.targetLight.edgeA = pointA;
                    this.targetLight.edgeB = pointB;
                }
            });
        });
        this.targetLight.edgeA.x -= (this.targetLight.center.x - this.targetLight.edgeA.x) * 2;
        this.targetLight.edgeA.y -= (this.targetLight.center.y - this.targetLight.edgeA.y) * 2;
        this.targetLight.edgeB.x -= (this.targetLight.center.x - this.targetLight.edgeB.x) * 2;
        this.targetLight.edgeB.y -= (this.targetLight.center.y - this.targetLight.edgeB.y) * 2;
    }

    deselectElement() {
        this.resetLight(this.targetLight);
    }

    getElementCorners(element) {
        let rect = element.getBoundingClientRect();
        return [
            new Point(rect.left - this.lightSpread, rect.top - this.lightSpread),
            new Point(rect.right + this.lightSpread, rect.top - this.lightSpread),
            new Point(rect.left - this.lightSpread, rect.bottom + this.lightSpread),
            new Point(rect.right + this.lightSpread, rect.bottom + this.lightSpread)
        ];
    }

    getCenter() {
        let rect = this.containerElement.getBoundingClientRect();
        return new Point(rect.left + rect.width / 2, rect.top + rect.height / 2);
    }

    resetLight(light) {
        light.center = this.getCenter();
        light.edgeA = this.getCenter();
        light.edgeB = this.getCenter();
    }

    updateLight() {
        this.light.edgeA.x = this.light.edgeA.x + (this.targetLight.edgeA.x - this.light.edgeA.x) / this.animationDelay;
        this.light.edgeA.y = this.light.edgeA.y + (this.targetLight.edgeA.y - this.light.edgeA.y) / this.animationDelay;
        this.light.edgeB.x = this.light.edgeB.x + (this.targetLight.edgeB.x - this.light.edgeB.x) / this.animationDelay;
        this.light.edgeB.y = this.light.edgeB.y + (this.targetLight.edgeB.y - this.light.edgeB.y) / this.animationDelay;
    }

    drawLight() {
        this.canvasElements.forEach(canvasElement => {
            let canvas = canvasElement.getContext('2d');
            let gradient = this.getGradient(canvas);
            canvas.clearRect(0, 0, canvasElement.width, canvasElement.height);
            canvas.fillStyle = gradient;
            canvas.filter = 'blur(' + this.lightBlur + 'px)';
            canvas.beginPath();
            canvas.moveTo(this.light.center.x, this.light.center.y);
            canvas.lineTo(this.light.edgeA.x, this.light.edgeA.y);
            canvas.lineTo(this.light.edgeB.x, this.light.edgeB.y);
            canvas.closePath();
            canvas.fill();
        });
    }

    getGradient(canvas) {
        let gradient = canvas.createLinearGradient(
            this.light.center.x,
            this.light.center.y,
            (this.light.edgeA.x + this.light.edgeB.x) / 2,
            (this.light.edgeA.y + this.light.edgeB.y) / 2
        );
        this.lightColor.a = 1;
        gradient.addColorStop(0, this.lightColor.toString());
        this.lightColor.a = 0;
        gradient.addColorStop(1, this.lightColor.toString());
        this.lightColor.a = 1;
        return gradient;
    }

}

// Runner

let lightController = new LightController();
lightController.containerElement = document.querySelector(CONTAINER_ELEMENT_SELECTOR);
lightController.mainElement = document.querySelector(MAIN_ELEMENT_SELECTOR);
lightController.itemElements = document.querySelectorAll(ITEM_ELEMENT_SELECTOR);
lightController.lightColor = new Color(LIGHT_COLOR[0], LIGHT_COLOR[1], LIGHT_COLOR[2]);
lightController.lightIntensity = LIGHT_INTENSITY;
lightController.lightSpread = LIGHT_SPREAD;
lightController.lightBlur = LIGHT_BLUR;
lightController.animationDelay = ANIMATION_DELAY;
lightController.run();